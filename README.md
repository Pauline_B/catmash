# CatMash

This project was generated with [Angular CLI](https://github.com/angular/angular-cli) version 8.0.4.

## Install App

To install all packages, you need to run the command: `npm intall`

## Development server

Run `ng serve` for a dev server. Navigate to `http://localhost:4200/`. The app will automatically reload if you change any of the source files.

## Running unit tests

Run `ng test` to execute the unit tests via [Karma](https://karma-runner.github.io).
It will open a new Karma window, and let you see tests results.

## Running functional tests

Run `npm run cypress:open` to execite the functional Cypress tests.
It will open a new Cypress window, and let you choose which tests you want to run.
