import { NgModule } from "@angular/core";
import { RouterModule } from "@angular/router";
import { ContainerComponent } from "./container/container.component";
import { VoteResultsComponent } from "./vote-results/vote-results.component";

@NgModule({
  imports: [
    RouterModule.forRoot([
      {
        path: "vote",
        component: VoteResultsComponent
      },
      {
        path: "",
        component: ContainerComponent
      }
    ])
  ],
  exports: [RouterModule]
})
export class AppRoutingModule {}
