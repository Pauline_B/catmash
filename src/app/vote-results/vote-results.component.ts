import { Component, OnInit, OnDestroy } from "@angular/core";
import { CatsService } from "../services/cats.service";
import { Cat } from "../models/cat";
import { Subscription } from "rxjs";

@Component({
  selector: "app-vote-results",
  templateUrl: "./vote-results.component.html",
  styleUrls: ["./vote-results.component.scss"]
})
export class VoteResultsComponent implements OnInit, OnDestroy {
  cats: Cat[][];

  catsSubscription: Subscription;

  constructor(private catsService: CatsService) {}

  ngOnInit() {
    this.subscriptionInit();
    this.catsService.getCatsScore();
    this.catsService.emitCatsSubject();
  }

  ngOnDestroy() {
    this.catsSubscription.unsubscribe();
  }

  subscriptionInit() {
    this.catsSubscription = this.catsService.catsSubject.subscribe(
      (cats: Cat[][]) => {
        this.cats = cats;
      }
    );
  }
}
