import { async, ComponentFixture, TestBed } from "@angular/core/testing";
import { VoteResultsComponent } from "./vote-results.component";
import { HttpClientTestingModule } from "@angular/common/http/testing";

describe("VoteResultsComponent", () => {
  let component: VoteResultsComponent;
  let fixture: ComponentFixture<VoteResultsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      imports: [HttpClientTestingModule],
      declarations: [VoteResultsComponent]
    }).compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(VoteResultsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it("should create", () => {
    expect(component).toBeTruthy();
  });
});
