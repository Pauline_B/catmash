import { TestBed, ComponentFixture } from "@angular/core/testing";
import { CatsService } from "./cats.service";
import {
  HttpClientTestingModule,
  HttpTestingController
} from "@angular/common/http/testing";
import { CatComponent } from "../cat/cat.component";
import { VoteResultsComponent } from "../vote-results/vote-results.component";
import { Cat } from "../models/cat";

describe("CatsService", () => {
  let catsService: CatsService;

  let httpTestingController: HttpTestingController;

  let catComponent: CatComponent;
  let catFixture: ComponentFixture<CatComponent>;

  let voteComponent: VoteResultsComponent;
  let fixture: ComponentFixture<VoteResultsComponent>;

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [HttpClientTestingModule],
      declarations: [CatComponent, VoteResultsComponent],
      providers: [CatsService]
    }).compileComponents();

    catsService = TestBed.get(CatsService);

    fixture = TestBed.createComponent(VoteResultsComponent);
    voteComponent = fixture.componentInstance;
    fixture.detectChanges();

    catFixture = TestBed.createComponent(CatComponent);
    catComponent = catFixture.componentInstance;
    catFixture.detectChanges();
  });

  it("should be created", () => {
    catsService = TestBed.get(CatsService);
    expect(catsService).toBeTruthy();
  });

  it("should return cats ordered by descendant score", () => {
    const cats = [
      {
        url: "http://25.media.tumblr.com/tumblr_m4bgd9OXmw1qioo2oo1_500.jpg",
        id: "bmp",
        score: 1
      },
      {
        url: "http://24.media.tumblr.com/tumblr_lzxok2e2kX1qgjltdo1_1280.jpg",
        id: "c8a",
        score: 5
      },
      {
        url: "http://25.media.tumblr.com/tumblr_m33r7lpy361qzi9p6o1_500.jpg",
        id: "3kj",
        score: 0
      },
      {
        url: "http://25.media.tumblr.com/tumblr_m2p6dxhxul1qdvz31o1_500.jpg",
        id: "9pu",
        score: 6
      },
      {
        url: "http://24.media.tumblr.com/tumblr_m1ku66jPWV1qze0hyo1_400.jpg",
        id: "aca",
        score: 4
      },
      {
        url: "http://25.media.tumblr.com/tumblr_m4pwa9EXE41r6jd7fo1_500.jpg",
        id: "ebk",
        score: 2
      }
    ];
    catsService.setCats(cats);
    expect(catsService.sortCatByScore()).toEqual([
      {
        url: "http://25.media.tumblr.com/tumblr_m2p6dxhxul1qdvz31o1_500.jpg",
        id: "9pu",
        score: 6
      },
      {
        url: "http://24.media.tumblr.com/tumblr_lzxok2e2kX1qgjltdo1_1280.jpg",
        id: "c8a",
        score: 5
      },

      {
        url: "http://24.media.tumblr.com/tumblr_m1ku66jPWV1qze0hyo1_400.jpg",
        id: "aca",
        score: 4
      },
      {
        url: "http://25.media.tumblr.com/tumblr_m4pwa9EXE41r6jd7fo1_500.jpg",
        id: "ebk",
        score: 2
      },
      {
        url: "http://25.media.tumblr.com/tumblr_m4bgd9OXmw1qioo2oo1_500.jpg",
        id: "bmp",
        score: 1
      },
      {
        url: "http://25.media.tumblr.com/tumblr_m33r7lpy361qzi9p6o1_500.jpg",
        id: "3kj",
        score: 0
      }
    ]);
  });

  it("should send correctly emitted cats to VoteResultsComponent with rxjs", () => {
    const cats: Cat[][] = [
      [
        {
          url: "http://25.media.tumblr.com/tumblr_m4bgd9OXmw1qioo2oo1_500.jpg",
          id: "bmp",
          score: 1
        }
      ],
      [
        {
          url: "http://25.media.tumblr.com/tumblr_m4pwa9EXE41r6jd7fo1_500.jpg",
          id: "ebk",
          score: 2
        }
      ]
    ];
    catsService.setResultCats(cats);
    voteComponent.subscriptionInit();
    catsService.emitCatsSubject();
    expect(voteComponent.cats).toEqual(cats);
  });

  it("should convert Cat[] to Cat[][] with row.length = 5", () => {
    const cats: Cat[] = [
      {
        url: "http://25.media.tumblr.com/tumblr_m2p6dxhxul1qdvz31o1_500.jpg",
        id: "9pu",
        score: 6
      },
      {
        url: "http://24.media.tumblr.com/tumblr_lzxok2e2kX1qgjltdo1_1280.jpg",
        id: "c8a",
        score: 5
      },

      {
        url: "http://24.media.tumblr.com/tumblr_m1ku66jPWV1qze0hyo1_400.jpg",
        id: "aca",
        score: 4
      },
      {
        url: "http://25.media.tumblr.com/tumblr_m4pwa9EXE41r6jd7fo1_500.jpg",
        id: "ebk",
        score: 2
      },
      {
        url: "http://25.media.tumblr.com/tumblr_m4bgd9OXmw1qioo2oo1_500.jpg",
        id: "bmp",
        score: 1
      },
      {
        url: "http://25.media.tumblr.com/tumblr_m33r7lpy361qzi9p6o1_500.jpg",
        id: "3kj",
        score: 0
      }
    ];
    catsService.setCats(cats);
    expect(catsService.getCatsByFive()).toEqual([
      [
        {
          url: "http://25.media.tumblr.com/tumblr_m2p6dxhxul1qdvz31o1_500.jpg",
          id: "9pu",
          score: 6
        },
        {
          url: "http://24.media.tumblr.com/tumblr_lzxok2e2kX1qgjltdo1_1280.jpg",
          id: "c8a",
          score: 5
        },
        {
          url: "http://24.media.tumblr.com/tumblr_m1ku66jPWV1qze0hyo1_400.jpg",
          id: "aca",
          score: 4
        },
        {
          url: "http://25.media.tumblr.com/tumblr_m4pwa9EXE41r6jd7fo1_500.jpg",
          id: "ebk",
          score: 2
        },
        {
          url: "http://25.media.tumblr.com/tumblr_m4bgd9OXmw1qioo2oo1_500.jpg",
          id: "bmp",
          score: 1
        }
      ],
      [
        {
          url: "http://25.media.tumblr.com/tumblr_m33r7lpy361qzi9p6o1_500.jpg",
          id: "3kj",
          score: 0
        }
      ]
    ]);
  });

  it("should add one point to the selected cat", () => {
    const cats: Cat[] = [
      {
        url: "http://25.media.tumblr.com/tumblr_m2p6dxhxul1qdvz31o1_500.jpg",
        id: "9pu",
        score: 6
      },
      {
        url: "http://24.media.tumblr.com/tumblr_lzxok2e2kX1qgjltdo1_1280.jpg",
        id: "c8a",
        score: 5
      }
    ];
    catsService.setCats(cats);
    catsService.addOnePoint({
      url: "http://25.media.tumblr.com/tumblr_m2p6dxhxul1qdvz31o1_500.jpg",
      id: "9pu",
      score: 6
    });
    expect(cats[0].score).toBe(7);
  });

  it("should emit left and right Cats", () => {
    const cats: Cat[] = [
      {
        url: "http://25.media.tumblr.com/tumblr_m2p6dxhxul1qdvz31o1_500.jpg",
        id: "9pu",
        score: 6
      }
    ];
    catsService.setCats(cats);
    catComponent.initLeftCatSubscription();
    catComponent.initRightCatSubscription(
      (cat: Cat) => (catComponent.rightCat = cat)
    );
    catsService.emitCatLeftSubject();
    catsService.emitCatRightSubject();
    expect(catComponent.leftCat).toEqual(cats[0]);
    expect(catComponent.rightCat).toEqual(cats[0]);
  });
});
