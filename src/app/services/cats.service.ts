import { Injectable } from "@angular/core";
import { HttpClient } from "@angular/common/http";
import { Cat } from "../models/cat";
import { Subject } from "rxjs";

@Injectable({
  providedIn: "root"
})
export class CatsService {
  catSubject = new Subject<Cat>();

  catsSubject = new Subject<Cat[][]>();
  catLeftSubject = new Subject<Cat>();
  catRightSubject = new Subject<Cat>();

  private isLoad = false;

  private cats: Cat[];
  private resultCats: Cat[][];

  constructor(private http: HttpClient) {}

  emitCatsSubject() {
    this.catsSubject.next(this.resultCats);
  }

  emitCatLeftSubject() {
    this.catLeftSubject.next(this.getRandomCat());
  }

  emitCatRightSubject() {
    this.catRightSubject.next(this.getRandomCat());
  }

  getRandomCat() {
    return this.cats[Math.floor(Math.random() * this.cats.length)];
  }

  getCats() {
    return this.http.get("../assets/data/cats.json");
  }

  setCats(cats: Cat[]): void {
    this.cats = cats;
  }

  setResultCats(resultCats: Cat[][]): void {
    this.resultCats = resultCats;
  }

  initCats(callback) {
    this.getCats().subscribe((data: Cat[]) => {
      this.cats = [];
      data.forEach(cat => {
        this.cats.push(new Cat(cat.id, cat.url));
      });
      this.isLoad = true;
      callback();
    });
  }

  getIsLoad(): boolean {
    return this.isLoad;
  }

  addOnePoint(cat: Cat) {
    cat.score++;
    const index = this.cats.findIndex(c => c.id === cat.id);
    this.cats[index] = cat;
  }

  getCatsScore() {
    if (this.cats === undefined) {
      this.initCats(() => this.getCatsByFive());
    } else {
      this.getCatsByFive();
    }
  }

  getCatsByFive() {
    this.sortCatByScore();
    const maxLengthIntoIntermediarCats = 5;
    this.resultCats = this.cats.reduce((resultArray, cat, index) => {
      const chunkIndex = Math.floor(index / maxLengthIntoIntermediarCats);
      if (!resultArray[chunkIndex]) {
        resultArray[chunkIndex] = [];
      }
      resultArray[chunkIndex].push(cat);
      return resultArray;
    }, []);
    return this.resultCats;
  }

  sortCatByScore(): Cat[] {
    return (this.cats = this.cats.sort((a: Cat, b: Cat) => {
      if (a.score > b.score) {
        return -1;
      } else if (a.score < b.score) {
        return 1;
      }
      return 0;
    }));
  }
}
