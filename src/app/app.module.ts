import { BrowserModule } from "@angular/platform-browser";
import { NgModule } from "@angular/core";
import { HttpClientModule } from "@angular/common/http";

import { AppComponent } from "./app.component";
import { CatComponent } from "./cat/cat.component";
import { VoteResultsComponent } from "./vote-results/vote-results.component";
import { ContainerComponent } from "./container/container.component";
import { AppRoutingModule } from "./app-router.module";

@NgModule({
  declarations: [
    AppComponent,
    CatComponent,
    VoteResultsComponent,
    ContainerComponent
  ],
  imports: [BrowserModule, HttpClientModule, AppRoutingModule],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule {}
