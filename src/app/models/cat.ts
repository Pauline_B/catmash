export class Cat {
  url: string;
  id: string;
  score: number;

  constructor(id: string, url: string) {
    this.id = id;
    this.url = url;
    this.score = 0;
  }
}
