import { Component, OnInit, OnDestroy } from "@angular/core";
import { CatsService } from "../services/cats.service";
import { Subscription } from "rxjs";
import { Cat } from "../models/cat";

@Component({
  selector: "app-cat",
  templateUrl: "./cat.component.html",
  styleUrls: ["./cat.component.scss"]
})
export class CatComponent implements OnInit, OnDestroy {
  catLeftSubscription: Subscription;
  catRightSubscription: Subscription;

  leftCat: Cat;
  rightCat: Cat;

  display = "block";

  constructor(private catsService: CatsService) {}

  ngOnInit() {
    if (!this.catsService.getIsLoad()) {
      this.catsService.initCats(() => this.init());
    } else {
      this.init();
    }
  }

  init() {
    this.showCats();
    this.catsService.emitCatLeftSubject();
    this.catsService.emitCatRightSubject();
    this.catsService.getCatsScore();
    this.catsService.emitCatsSubject();
  }

  ngOnDestroy() {
    this.catLeftSubscription.unsubscribe();
    this.catRightSubscription.unsubscribe();
  }

  showCats() {
    this.initLeftCatSubscription();
    this.initRightCatSubscription((cat: Cat) => {
      if (cat.id === this.leftCat.id) {
        this.catsService.emitCatRightSubject();
      } else {
        this.rightCat = cat;
      }
    });
  }

  initLeftCatSubscription() {
    this.catLeftSubscription = this.catsService.catLeftSubject.subscribe(
      (cat: Cat) => {
        this.leftCat = cat;
      }
    );
  }

  initRightCatSubscription(callback) {
    this.catRightSubscription = this.catsService.catRightSubject.subscribe(
      callback
    );
  }

  onAddOnePoint(selectedCat: string) {
    if (selectedCat === "left") {
      this.catsService.addOnePoint(this.leftCat);
    } else {
      this.catsService.addOnePoint(this.rightCat);
    }
    this.display = "none";
    this.catsService.emitCatLeftSubject();
    this.catsService.emitCatRightSubject();
  }
}
