describe("Testing that the cat component", () => {
  beforeEach(() => {
    cy.server();
    cy.visit("http://localhost:4200/");
  });

  it("should allow the user to click on left cat", () => {
    cy.get("#left-cat").click();
  });

  it("should allow the user to click on right cat", () => {
    cy.get("#right-cat").click();
  });

  it("should make the button unaccessible when the user didn't click on a cat before", () => {
    cy.get("#next").should("not.be.visible");
  });

  it('should allow the user to click on the "Présenter les chats préférés" if the user clicked on right cat before', () => {
    cy.get("#right-cat").click();
    cy.get("#next").click();
    cy.wait(400);
    cy.get("#results").should("be.visible");
  });
});
