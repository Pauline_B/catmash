describe("Testing the list showed at connexion ", () => {
  beforeEach(() => {
    cy.server();
    cy.visit("http://localhost:4200/");
    cy.get("#left-cat").click();
    cy.wait(400);
    cy.get("#next").click();
    cy.wait(400);
  });

  it("should show cats", () => {
    cy.get("#cats-results").should("be.visible");
  });

  it("should show a button that allows the user to go back to the vote page", () => {
    cy.get("#return").click();
    cy.wait(400);
    cy.get("#cats-choice").should("be.visible");
  });
});
